from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from django.apps import apps
from .apps import HomepepageConfig

# Create your tests here.
class Story8_test(TestCase):

    def test_page(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'index.html')
    
    # def test_json_url(self):
    #     response = Client().get('/search/')
    #     self.assertEqual(response.status_code,200)
    
    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Try to search BOOKS!",html_response)

    def test_apps(self):
        self.assertEqual(HomepepageConfig.name, 'homepepage')
        self.assertEqual(apps.get_app_config('homepepage').name, 'homepepage')
