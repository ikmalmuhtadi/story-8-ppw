from django.apps import AppConfig


class HomepepageConfig(AppConfig):
    name = 'homepepage'
