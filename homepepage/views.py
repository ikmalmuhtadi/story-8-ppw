from django.shortcuts import render
from django.http import JsonResponse
import json
import requests


# Create your views here.
def index(request):
    return render(request, 'index.html')

def search(request):
    q = request.GET['q']
    
    # json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q)
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + q
    req = requests.get(url_tujuan)

    x = json.loads(req.content)

    return JsonResponse(x, safe=False)
